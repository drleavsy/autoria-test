package tests;

import org.junit.Test;

public class LoginTest extends BaseTest{

    @Test //(priority = 1, description="Invalid Login Scenario with wrong username and password.")
    public void LoginWithExistingUser() {
        // Proxy pattern for catching annoying pop up , doesn't work yet
//        try {
//            MyPageFactory.initElements(driverPublic, HomePage.class);
//        }
//        catch (Throwable throwable) {
//            throwable.printStackTrace();
//        }

        // Go to HomePage and click the Login icon and navigate to Login page
        homePage.goToLoginPage();
    }

    @Test //(priority = 1, description="Invalid Login Scenario with wrong username and password.")
    public void ClickMenuItemEverythingForAuto() {
        // click the link "Everything for auto" from top Menu list
        homePage.goToTopMenuEverythingForAuto();
    }
//    @Test
//    public void UpperMenuEverythigForAutoCalculateAveragePrice() {
//
//        HomePage homePageObject = PageFactory.initElements(driverSingleton, HomePage.class).goToHomePage();
//
//        Menu menuObject = PageFactory.initElements(driverSingleton, Menu.class);
//
//        int numberOfMenuItems = menuObject.getMenuItemsCount();
//    }
}

package tests;

import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import pages.HomePage;
import utilities.SingletonBrowserChrome;

public class BaseTest {

    protected WebDriver driverSingleton;
    protected HomePage homePage;
    private static SingletonBrowserChrome singletonInstanceChrome = null;

    @Before
    public void testSetUp(){
        singletonInstanceChrome = SingletonBrowserChrome.getInstanceOfSingletonBrowser();
        driverSingleton = singletonInstanceChrome.getDriver();
        homePage = PageFactory.initElements(driverSingleton, HomePage.class);
    }

    @After
    public void  testTearDown(){
        singletonInstanceChrome.quitDriver();
    }
}

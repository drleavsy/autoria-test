package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import utilities.PropertyManager;

public class HomePage extends BasePage{

    // Constructor
    public HomePage(WebDriver driver) {
        // inherit instance of driver from BasePage
        super(driver);
        // use the instance of the singleton driver to open home page
        driverLocal.get(baseUrl);
        // Each time we go to homepage the popup message appears
        VotePopUp popUpObject = PageFactory.initElements(driverLocal, VotePopUp.class);
        // Close the popup window if found
        if(popUpObject.isPopUpWindow()){
            popUpObject.closePopUpWindow();
        }
    }

    private Menu topMenuList;

    // Page variables
    private String baseUrl = PropertyManager.getInstance().getBaseUrl();//"https://auto.ria.com/";

    // go to homepage method
    public HomePage goToHomePage(){
        return this;
    }

    // go to login page method by clicking on link
    public LoginPage goToLoginPage(){
        return PageFactory.initElements(driverLocal, LoginPage.class).clickLoginPageLink();
    }

    // go to login page method by clicking on link
    public EverythingForAutoPage goToTopMenuEverythingForAuto(){
        topMenuList = new Menu();
        topMenuList.ClickItemFromMenuListByName(topMenuList.menuEverythingForAuto);
        return PageFactory.initElements(driverLocal, EverythingForAutoPage.class);
    }
}

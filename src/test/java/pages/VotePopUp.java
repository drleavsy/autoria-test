package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import tests.BasePopUp;

public class VotePopUp extends BasePopUp {

    public VotePopUp(WebDriver driver) {
        super(driver);
    }

    @FindBy(css="a.close.unlink")
    WebElement closeLink;

    @FindBy(className="pupup-al-wrapper")
    WebElement popUpElement;

    // method for verifying pop up window
    public boolean isPopUpWindow(){
        if(popUpElement.isDisplayed()){
            return true;
        }
        return false;
    }
    // method for closing the pop up
    public void closePopUpWindow(){
        try {
            closeLink.click();
        }
        catch(Exception e) {
            System.out.printf("Exception occurred during closing pop up window: %s\n", e.getMessage());
        }
    }
}

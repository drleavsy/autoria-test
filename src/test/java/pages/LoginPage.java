package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import utilities.PropertyManager;

public class LoginPage extends BasePage{

    // Constructor
    public LoginPage(WebDriver driver) {
        super(driver);
    }

    // Locators
    By usersEmail = By.id("emailloginform-email");
    By userPassword = By.id("emailloginform-password");
    By rememberMeLabel = By.className("label.label-ckeck.sub");
    By rememberMeCheckBox = By.id("rememberme_checkbox_id");
    By bigGreenButton = By.className("button.button.large.green.g-recaptcha");
    By forgotPassword = By.id("remember_password");

    // Locators
    By loginLink = By.ByCssSelector.cssSelector("#header a[href='https://auto.ria.com/login.html']");

    private String loginUrl = "https://auto.ria.com/login.html";
    //Test Data
    private String wrongUsername = PropertyManager.getInstance().getProperUsername();
    private String wrongPassword = PropertyManager.getInstance().getProperPassword();

    // go to login page method by clicking on link
    public LoginPage clickLoginPageLink(){
        click(loginLink);
        return this;//new LoginPage(driverLocal);
    }

    // go to login page method via URL
    public LoginPage goLoginPageViaURL(){
        driverLocal.get(loginUrl);
        return this;//new LoginPage(driverLocal);
    }


}

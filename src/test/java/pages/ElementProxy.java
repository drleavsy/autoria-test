package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import utilities.SingletonBrowserChrome;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

public class ElementProxy implements InvocationHandler {

    private final WebElement element;

    public ElementProxy(WebElement element){
        this.element = element;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        // before invoking actual method check for popup
        this.checkForPopUpAndKill();
        // at this point the pop up has to be closed, element action can be called safely now
        return method.invoke(element, args);
    }

    private void checkForPopUpAndKill(){
        VotePopUp popUpInstance = PageFactory.initElements(
                SingletonBrowserChrome.getInstanceOfSingletonBrowser().getDriver(),
                VotePopUp.class);

        if (popUpInstance.isPopUpWindow()){
            popUpInstance.closePopUpWindow();
        }
    }
}

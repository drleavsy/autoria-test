package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import utilities.SingletonBrowserChrome;

import java.util.List;

public class Menu {

    private WebDriver localDriver;
    private List<WebElement> menuElements;

    // Constructor
    public Menu() {
        localDriver = SingletonBrowserChrome.getInstanceOfSingletonBrowser().getDriver();
        menuElements = localDriver.findElements(menuList);
    }

    // Strings
    String menuEverythingForAuto = "Все для авто";

    // Locators
    private By menuList = By.xpath("//ul[contains(@class, 'nav')]/li");


    // click the menu item by its name
    public void ClickItemFromMenuListByName(String menuName){
        for(WebElement menuItem : menuElements){
            if(menuName.equals(menuItem.getText())){
                menuItem.click();
            }
        }
    }

    // get the number of links in menu
    public int getMenuItemsCount(){
        return menuElements.size();
    }

}

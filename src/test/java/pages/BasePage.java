package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

public class BasePage {
    protected WebDriver driverLocal;
    protected WebDriverWait waitLocal;

    // Constructor
    public BasePage(WebDriver driver)
    {
        this.driverLocal = driver;
        waitLocal = new WebDriverWait(driver, 15);
    }
    // Wrapper for visibility wait
    public void waitVisibility(By elementBy){
        waitLocal.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(elementBy));
    }
    // click element with wait for visibility
    public void click(By elementBy){
        waitVisibility(elementBy);
        driverLocal.findElement(elementBy).click();
    }
    // write text to element with wait
    public void writeText(By elementBy, String text){
        waitVisibility(elementBy);
        driverLocal.findElement(elementBy).sendKeys(text);
    }
    // read text
    public String readText(By elementBy){
        waitVisibility(elementBy);
        return driverLocal.findElement(elementBy).getText();
    }
    // Assert title contains text
    public void isTitleEqualTo(String text){
        Assert.assertEquals(text, driverLocal.getTitle());
    }
    // Assert if url matches text
    public void isUrlEqualTo(String text){
        Assert.assertEquals(text, driverLocal.getCurrentUrl());
    }

    //

}

package utilities;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import java.lang.reflect.Field;

public class MyPageFactory {
    public static <T> void initElements(WebDriver driver, T pageObject) throws Throwable {
        // initialize the page object
        PageFactory.initElements(driver, pageObject);

        // access all WebElements on the page and create wrapper
        for(Field fieldIndex:pageObject.getClass().getDeclaredFields()){

            if(fieldIndex.getType().equals(WebElement.class)){
                boolean accessible = fieldIndex.canAccess(pageObject);
                fieldIndex.setAccessible(true);
                // reset the webelement with proxy object
                fieldIndex.set(pageObject, ElementGuard.guard((WebElement) fieldIndex.get(pageObject)));
            }
        }
    }
}

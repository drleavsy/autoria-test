package utilities;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class PropertyManager {

    private static PropertyManager instance;
    private static String filePath = System.getProperty("user.dir")+
            "\\src\\test\\resources\\configuration.properties";
    private static String homeUrl;
    private static String properUsername;
    private static String properPassword;


    // Create a Singleton
    public static PropertyManager getInstance(){
        if(instance==null){
            instance = new PropertyManager();
        }
        return instance;
    }

    // Get config information from file
    private void getConfiguration()
    {
        Properties propertiesInstance = new Properties();
        try {
            propertiesInstance.load(new FileInputStream(filePath));
        }
        catch(IOException e) {
            System.out.println("Configuration file was not found");
        }
        // Get properties from file
        homeUrl = propertiesInstance.getProperty("baseUrl");
        properUsername = propertiesInstance.getProperty("properUsername");
        properPassword = propertiesInstance.getProperty("properPassword");
    }

    public String getBaseUrl(){
        return homeUrl;
    }

    public String getProperUsername(){
        return properUsername;
    }

    public String getProperPassword(){
        return properPassword;
    }
}

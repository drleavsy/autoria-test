package utilities;

import org.openqa.selenium.WebElement;
import pages.ElementProxy;

import java.lang.reflect.Proxy;

public class ElementGuard {
    public static WebElement guard(WebElement element){

        ElementProxy proxy = new ElementProxy(element);

        return (WebElement) Proxy.newProxyInstance(
                ElementProxy.class.getClassLoader(),
                new Class[] {WebElement.class},
                proxy);
    }
}

package utilities;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class SingletonBrowserChrome {

    private static SingletonBrowserChrome instanceOfSingletonBrowser = null;
    //private static String browser = ConfigurationManager.AppSettings["browser"];

    private WebDriver driver;

    // Constructor
    private SingletonBrowserChrome(){
        // Set path for chrome driver
        System.setProperty("webdriver.chrome.driver", "chromedriver_win32/chromedriver.exe");
        // Define chrome options
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--start-maximized", "--disable-cache");
        // Create instance of chrome driver with defined options
        driver = new ChromeDriver(options);
    }

    // Method for creating instance of class
    public static SingletonBrowserChrome getInstanceOfSingletonBrowser(){
        if(instanceOfSingletonBrowser==null){
            instanceOfSingletonBrowser = new SingletonBrowserChrome();
        }
        return instanceOfSingletonBrowser;
    }

    // get driver method
    public WebDriver getDriver(){
        return driver;
    }

    // quit driver
    public void quitDriver(){
        driver.quit();
        instanceOfSingletonBrowser = null;
    }
}
